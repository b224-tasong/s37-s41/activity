// Basic Express Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();
const port = process.env.PORT || 7000;
// const port = 7000;


mongoose.connect("mongodb+srv://Ricowafu:admin123@224-rico.tipkxxe.mongodb.net/course-booking-API?retryWrites=true&w=majority",

		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}

	);


let db = mongoose.connection;
db.on("error", () => console.error("Connection error."));
db.once("open", () => console.log("Connected to MongoDB."));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI
app.use("/users", userRoutes);
app.use("/users", courseRoutes);

app.listen(port, () => {console.log(`API is now running at port ${port}`)});