const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");



// router.post("/addCourse", (req, res) =>{
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })


// router.post("/addCourse", auth.verify, (req, res) => {

// 	const courseData = auth.decode(req.headers.authorization);
// 	// console.log(req.headers.authorization);
// 	// console.log(courseData.id);
// 	courseController.checkUserisAdmin({id: courseData.id}).then(resultFromController => res.send(resultFromController))
// })

router.post("/addCourse", auth.verify, (req, res) => {

	const courseData = auth.decode(req.headers.authorization);

	console.log(courseData.isAdmin);
	if (courseData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return false
	}

	
})



module.exports = router